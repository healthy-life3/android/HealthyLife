package ru.nsu.healthylife

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import dagger.hilt.android.AndroidEntryPoint
import ru.nsu.healthylife.fragments.WelcomeFragment
import ru.nsu.healthylife.models.RegistrationModel
import ru.nsu.healthylife.models.Session

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {
    private lateinit var viewModel: RegistrationModel
    private lateinit var session: Session

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity_layout)
        viewModel = ViewModelProvider(this).get(
            RegistrationModel::class.java
        )
        session = ViewModelProvider(this).get(
            Session::class.java
        )
        viewModel.setFragmentManager(supportFragmentManager)
        replaceFragment(
            R.id.placeHolder,
            WelcomeFragment.newInstance()
        )
    }

    private fun replaceFragment(containerViewId: Int, fragment: Fragment) {
        supportFragmentManager
            .beginTransaction()
            .replace(containerViewId, fragment)
            .setReorderingAllowed(true)
            .addToBackStack(null)
            .commit()
    }

    override fun onBackPressed() {
        if (supportFragmentManager.backStackEntryCount > 1) {
            supportFragmentManager.popBackStack()
        }
//        super.onBackPressed()
//        TODO: IGNORE
    }
}