package ru.nsu.healthylife

class Constants {
    companion object {
        const val BAD_REQUEST_CODE = 400
        const val BAD_REQUEST = "Bad request"

        const val INVALID_CREDENTIALS_CODE = 401
        const val INVALID_CREDENTIALS = "Invalid email or password"

        const val FORBIDDEN_CODE = 403
        const val FORBIDDEN = "Forbidden"

        const val NOT_FOUND_CODE = 404
        const val NOT_FOUND = "Not found"

        const val SERVER_ERROR_CODE = 500
        const val SERVER_ERROR = "Internal server error"

        const val NOT_EQUAL_PASSWORDS = "Passwords are not equal!"
        const val INVALID_PHONE_NUMBER =
            "Invalid phone number format. Use russian numbers (+7-XXX-XXX-XX-XX)."
    }
}