package ru.nsu.healthylife.models

import android.app.AlertDialog
import android.content.Context
import android.view.View
import android.widget.AdapterView
import android.widget.EditText
import android.widget.Spinner
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.android.volley.VolleyError
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.Json
import ru.nsu.healthylife.Constants
import ru.nsu.healthylife.dto.*
import ru.nsu.healthylife.fragments.DoctorInfoFragment
import ru.nsu.healthylife.fragments.PatientInfoFragment
import ru.nsu.healthylife.fragments.WelcomeFragment
import ru.nsu.healthylife.services.AuthenticationService
import java.text.SimpleDateFormat
import java.util.*
import java.util.logging.Logger
import javax.inject.Inject
import kotlin.concurrent.thread

@HiltViewModel
class RegistrationModel @Inject constructor() : ViewModel() {
    private val log: Logger = Logger.getLogger(RegistrationModel::class.toString())

    companion object {
        const val DOCTOR_TYPE = 0
        const val PATIENT_TYPE = 1

        const val FAILED_TO_CONNECT_ERROR_MESSAGE = "Failed to connect to the server, check your Internet connection"
    }

    var type: Int = -1
    lateinit var fm: FragmentManager

    lateinit var firstName: String
    var middleName: String? = null
    lateinit var lastName: String

    lateinit var birthday: Date

    /**
     * 0 - Unknown,
     * 1 - Male,
     * 2 - Female,
     * 9 - Irrelevant
     * */
//    var gender: Int = 0
    lateinit var gender: String

    var isChild: Boolean = true

    lateinit var phoneNumber: PhoneNumber

    lateinit var login: String
    lateinit var password: String

    lateinit var email: String
    lateinit var snils: String

    /**
     * Passport info*/
    lateinit var documentSeries: String
    lateinit var documentNumber: String
    lateinit var placeOfIssue: String
    lateinit var dateOfIssue: Date

    /**
     * Address info*/
    lateinit var region: String
    lateinit var city: String
    lateinit var street: String

    //TODO zipcode
    private var zipcode: String = "123456"
    lateinit var houseNumber: String
    var apartmentNumber: Int = 0

    fun setFragmentManager(fragmentManager: FragmentManager) {
        fm = fragmentManager
    }

    fun setContactInfo(
        firstName: String,
        middleName: String?,
        lastName: String,
        email: String
    ) {
        this.firstName = firstName
        this.middleName = middleName
        this.lastName = lastName
        this.email = email
    }

    fun setIfCorrectPhoneNumber(
        phoneNumber: EditText,
        hasError: Boolean
    ): Boolean {
        val number = PhoneNumber(phoneNumber.text.toString().trim())
        return if (number.tryInit()) {
            this.phoneNumber = number
            hasError
        } else {
            phoneNumber.error = Constants.INVALID_PHONE_NUMBER
            true
        }
    }

    fun setIfCorrectBirthday(
        birthday: EditText,
        hasError: Boolean
    ): Boolean {
        val date = checkIfDateError(birthday.text.toString().trim())
        if (date != null) {
            this.birthday = date
            return hasError
        }
        birthday.error = "Incorrect date format, use pattern YYYY-MM-DD"
        return true
    }

    fun setIfCorrectDateOfIssue(
        dateOfIssue: EditText,
        hasError: Boolean
    ): Boolean {
        val date = checkIfDateError(dateOfIssue.text.toString().trim())
        if (date != null) {
            this.dateOfIssue = date
            return hasError
        }
        dateOfIssue.error = "Incorrect date format, use pattern YYYY-MM-DD"
        return true
    }

    private fun checkIfDateError(dateString: String): Date? {
        try {
            val date = SimpleDateFormat(
                "yyyy-MM-dd", Locale.US
            ).parse(dateString)

            if (date != null) {
                return date
            }
        } catch (ex: Exception) {
            println("Date parse exception: $ex")
        }
        return null
    }

    fun toAuthRequest(): AuthRequest {
        return AuthRequest(login, password)
    }

    fun toPatientRequest(): PatientRegistrationRequest {
        return PatientRegistrationRequest(
            lastName,
            firstName,
            middleName ?: "",
            PhoneNumberDto(
                phoneNumber.fullNumber,
                phoneNumber.countryCode,
                phoneNumber.subscriberNumber
            ),
            SimpleDateFormat("yyyy-MM-dd", Locale.US).format(birthday),
            password,
            email,
            gender,
            snils,
            PassportDto(
                documentSeries,
                documentNumber,
                placeOfIssue,
                SimpleDateFormat("yyyy-MM-dd", Locale.US).format(dateOfIssue)
            ),
            AddressDto(
                region,
                city,
                street,
                zipcode,
                houseNumber,
                apartmentNumber
            )
        )
    }

    fun toDoctorRequest(): DoctorRegistrationRequest {
        return DoctorRegistrationRequest(
            lastName,
            firstName,
            middleName ?: "",
            PhoneNumberDto(
                phoneNumber.fullNumber,
                phoneNumber.countryCode,
                phoneNumber.subscriberNumber
            ),
            SimpleDateFormat("yyyy-MM-dd", Locale.US).format(birthday),
            password,
            email,
            gender,
            PassportDto(
                documentSeries,
                documentNumber,
                placeOfIssue,
                SimpleDateFormat("yyyy-MM-dd", Locale.US).format(dateOfIssue)
            ),
            //DoctorProfessionDto(0, "")
            null
        )
    }

    private fun genderToString(gender: String) = when (gender) {
        "Пол" -> "UNKNOWN"
        "Мужчина" -> "MALE"
        "Женщина" -> "FEMALE"
        else -> "UNKNOWN"
    }

    fun setGenderSpinnerListener(spinner: Spinner) {
        spinner.onItemSelectedListener =
            object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(
                    parent: AdapterView<*>,
                    view: View?,
                    position: Int,
                    id: Long
                ) {
                    gender = genderToString(
                        parent.getItemAtPosition(position).toString()
                    )
                    //TODO remove logging
                    println("Spinner set value: $gender")
                }

                override fun onNothingSelected(parent: AdapterView<*>?) {
                    gender = genderToString("")
                    println("Spinner set value: $gender")
                }
            }
    }

    fun tryDoctorAuth(
        session: Session,
        containerId: Int,
        fragment: DoctorInfoFragment,
        passwordEditText: EditText
    ) {
        session.authenticationService.sendDoctorAuthRequest(
            toAuthRequest(),
            { response ->
                println("Auth response SUCCESS!")
                //TODO auth here
                session.jwtToken = Json.decodeFromString(response)
                session.type = DOCTOR_TYPE
                viewModelScope.launch {
                    thread {
                        session.storeToken()
                    }
                }
                println("jwtToken: ${session.jwtToken}")
                fm.let { for (i in 1..it.backStackEntryCount) it.popBackStack() }
                replaceFragment(
                    containerId,
                    fragment
                )
            },
            {
                println("Auth response ERROR: ${it.message}")
                if (isFailedToConnect(it)) {
                    setNoInternetError(passwordEditText.context)
                } else {
                    try{
                        reactOnStatusCode(
                            it.networkResponse.statusCode,
                            passwordEditText
                        )
                    } catch (ex : NullPointerException) {
                        println("RegistrationViewModel: tryDoctorAuth: ${ex.message}")
                    }
                }
            }
        )
    }

    fun tryPatientAuth(
        session: Session,
        containerId: Int,
        fragment: PatientInfoFragment,
        passwordEditText: EditText
    ) {
        session.authenticationService.sendPatientAuthRequest(
            toAuthRequest(),
            { response ->
                println("Auth response SUCCESS!")
                //TODO auth here
                session.jwtToken = Json.decodeFromString(response)
                session.type = PATIENT_TYPE
                viewModelScope.launch {
                    thread {
                        session.storeToken()
                    }
                }
                println("jwtToken: ${session.jwtToken}")
                fm.let { for (i in 1..it.backStackEntryCount) it.popBackStack() }
                replaceFragment(
                    containerId,
                    fragment
                )
            },
            {
                println("Auth response ERROR: ${it.message}")
                if (isFailedToConnect(it)) {
                    setNoInternetError(passwordEditText.context)
                } else {
                    try {
                        reactOnStatusCode(
                            it.networkResponse.statusCode,
                            passwordEditText
                        )
                    } catch (ex : NullPointerException) {
                        println("RegistrationViewModel: tryPatientAuth: ${ex.message}")
                    }
                }
            }
        )
    }

    private fun isFailedToConnect(error: VolleyError): Boolean {
        return error.networkResponse == null
    }

    private fun setNoInternetError(context: Context) {
        AlertDialog.Builder(context)
            .setMessage(FAILED_TO_CONNECT_ERROR_MESSAGE)
            .show()
    }

    fun tryDoctorRegistration(
        authenticationService: AuthenticationService,
        containerId: Int,
        fragment: WelcomeFragment,
        passwordEditText: EditText
    ) {
        authenticationService.sendDoctorRegistrationRequest(
            toDoctorRequest(),
            {
                println("Response SUCCESS!")
                fm.let { for (i in 1..it.backStackEntryCount) it.popBackStack() }
                replaceFragment(
                    containerId,
                    fragment
                )
            },
            {
                println("Response ERROR: ${it.message}")
                if (isFailedToConnect(it)) {
                    setNoInternetError(passwordEditText.context)
                } else {
                    try {
                        println(it.networkResponse.data)
                        reactOnStatusCode(
                            it.networkResponse.statusCode,
                            passwordEditText
                        )
                    } catch (ex: NullPointerException) {
                        println("RegistrationViewModel: tryDoctorRegistration: ${ex.message}")
                    }
                }
            }
        )
    }

    fun tryPatientRegistration(
        authenticationService: AuthenticationService,
        containerId: Int,
        fragment: WelcomeFragment,
        passwordEditText: EditText
    ) {//weak reference
        authenticationService.sendPatientRegistrationRequest(
            toPatientRequest(),
            {
                println("Response SUCCESS!")
                fm.let { for (i in 1..it.backStackEntryCount) it.popBackStack() }
                replaceFragment(
                    containerId,
                    fragment
                )
            },
            {
                println("Response ERROR: ${it.message}")
                if (isFailedToConnect(it)) {
                    setNoInternetError(passwordEditText.context)
                } else {
                    try {
                        println(it.networkResponse.data)
                        reactOnStatusCode(
                            it.networkResponse.statusCode,
                            passwordEditText
                        )
                    } catch (ex: NullPointerException) {
                        println("RegistrationViewModel: tryPatientRegistration: ${ex.message}")
                    }
                }

            }
        )
    }

    private fun reactOnStatusCode(statusCode: Int, field: EditText) {
        log.info("Response status code: $statusCode")
        field.error = when (statusCode) {
            Constants.BAD_REQUEST_CODE -> Constants.BAD_REQUEST
            Constants.INVALID_CREDENTIALS_CODE -> Constants.INVALID_CREDENTIALS
            Constants.FORBIDDEN_CODE -> Constants.FORBIDDEN
            Constants.NOT_FOUND_CODE -> Constants.NOT_FOUND
            Constants.SERVER_ERROR_CODE -> Constants.SERVER_ERROR
            else -> "Unknown error: $statusCode"
        }
    }

    private fun replaceFragment(containerViewId: Int, fragment: Fragment) {
        fm.beginTransaction()
            .replace(containerViewId, fragment)
            .setReorderingAllowed(true)
            .addToBackStack(null)
            .commit()
    }
}