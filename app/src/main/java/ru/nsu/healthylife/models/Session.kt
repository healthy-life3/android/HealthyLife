package ru.nsu.healthylife.models

import android.app.AlertDialog
import android.content.Context
import android.view.View
import android.view.WindowManager
import android.widget.EditText
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.Json
import ru.nsu.healthylife.Constants
import ru.nsu.healthylife.R
import ru.nsu.healthylife.db.DBRepository
import ru.nsu.healthylife.dto.DoctorProfileDto
import ru.nsu.healthylife.dto.JwtToken
import ru.nsu.healthylife.dto.PatientProfileDto
import ru.nsu.healthylife.services.AuthenticationService
import ru.nsu.healthylife.services.NetworkService
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject
import kotlin.concurrent.thread

@HiltViewModel
class Session @Inject constructor(
    val authenticationService: AuthenticationService,
    val networkService: NetworkService,
    val db: DBRepository,
    @ApplicationContext
    context: Context
) : ViewModel() {

    private val context: Context by lazy { context }

    companion object {
        const val DOCTOR_TYPE = 0
        const val PATIENT_TYPE = 1
        const val FAILED_TO_CONNECT_ERROR_MESSAGE = "Failed to connect to the server, check your Internet connection"
        const val FAILED_TO_LOAD_PROFILE_ERROR_MESSAGE = "Failed to load profile, retry"
    }

    var jwtToken: JwtToken? = null
    var type: Int? = null

    var profile: Profile? = null

    fun storeToken() {
        when (type) {
            DOCTOR_TYPE -> jwtToken!!.let { db.storeDoctorToken(it) }
            PATIENT_TYPE -> jwtToken!!.let { db.storePatientToken(it) }
        }
    }

    fun setProfileInfo(view: View) {
        view.findViewById<EditText>(R.id.lastNameEditText)
            .setText(profile?.lastName)
        view.findViewById<EditText>(R.id.middleNameEditText)
            .setText(profile?.middleName)
        view.findViewById<EditText>(R.id.firstNameEditText)
            .setText(profile?.firstName)
        view.findViewById<EditText>(R.id.genderEditText)
            .setText(localizeGender(profile?.gender))
        view.findViewById<EditText>(R.id.birthdayEditText)
            .setText(profile?.birthday)
        view.findViewById<EditText>(R.id.phoneEditText)
            .setText(profile?.phoneNumber?.fullNumber)
        view.findViewById<EditText>(R.id.emailEditText)
            .setText(profile?.email)
    }

    private fun localizeGender(genderString: String?): String {
        return when (genderString) {
            "MALE" -> "Мужчина"
            "FEMALE" -> "Женщина"
            else -> "Неизвестно"
        }
    }

    fun setName(editText: EditText) {
        loadProfile(editText.context) {
            editText.setText("${profile!!.lastName} ${profile!!.firstName} ${profile!!.middleName}")
        }
    }

    private fun loadProfile(context: Context, doWhenLoaded: () -> Unit) {
//        println("Inside loadProfile")
//        println("jwtToken: $jwtToken")
//        println("Type: $type")
        if (jwtToken == null || type == null) return

        if (type == DOCTOR_TYPE) {
            println("Type = Doctor")
            networkService.loadDoctorProfile(
                jwtToken!!,
                {
                    profile = Profile.fromDoctorDto(Json.decodeFromString(it.toString()))
                    doWhenLoaded()
                },
                {
                    viewModelScope.launch {

                        //TODO: handle the exception!!!
                        try {
                            AlertDialog.Builder(context.applicationContext)
                                .setMessage(FAILED_TO_LOAD_PROFILE_ERROR_MESSAGE)
                                .show()
                        } catch (ex : WindowManager.BadTokenException) {
                            println("Error: ${ex.message}")
                        } finally {
                            val thread = thread {
                                if (it.networkResponse != null &&
                                    it.networkResponse.statusCode == Constants.INVALID_CREDENTIALS_CODE
                                ) {
                                    db.deleteTokens()
                                }
                            }
                            withContext(Dispatchers.IO) {
                                thread.join()
                            }
                        }
                    }
                }
            )
        } else if (type == PATIENT_TYPE) {
            println("Type = Patient")
            networkService.loadPatientProfile(
                jwtToken!!,
                {
                    profile = Profile.fromPatientDto(Json.decodeFromString(it.toString()))
                    doWhenLoaded()
                },
                {
                    viewModelScope.launch {
                        //TODO: handle the exception!!!
                        try {
                            AlertDialog.Builder(context.applicationContext)
                                .setMessage(FAILED_TO_LOAD_PROFILE_ERROR_MESSAGE)
                                .show()
                        } catch (ex : WindowManager.BadTokenException) {
                            println("Error: ${ex.message}")
                        } finally {
                            val thread = thread {
                                if (it.networkResponse != null &&
                                    it.networkResponse.statusCode == Constants.INVALID_CREDENTIALS_CODE
                                ) {
                                    db.deleteTokens()
                                }
                            }
                            withContext(Dispatchers.IO) {
                                thread.join()
                            }
                        }

                    }
                }
            )
        }
    }

    fun tryGetAccess(): Boolean {
        val token = db.getToken()
        return if (token != null) {
            jwtToken = token.token
            type = token.type
            true
        } else {
            false
        }
    }

    fun dropAccess() {
        db.deleteTokens()
    }

    class Profile private constructor(
        var lastName: String,
        var firstName: String,
        var middleName: String,

        var gender: String,
        var email: String,
        var birthday: String,

        var phoneNumber: PhoneNumber,

        /**
         * Passport info*/
        var documentSeries: String,
        var documentNumber: String,
        var placeOfIssue: String,
        var dateOfIssue: Date?,

        /**
         * Patient info
         * */
        var snils: String?,

        /**
         * Address info*/
        var region: String?,
        var city: String?,
        var street: String?

        /**
         * Doctor info
         * */

    ) {

        companion object {
            const val UNKNOWN = 0
            const val MALE = 1
            const val FEMALE = 2
            const val IRRELEVANT = 9

            @JvmStatic
            fun fromDoctorDto(dto: DoctorProfileDto): Profile {
                return Profile(
                    dto.lastName,
                    dto.firstName,
                    dto.middleName,
                    dto.gender,
                    dto.email,
                    dto.birthday,
                    PhoneNumber(dto.phoneNumber.fullNumber),
                    dto.passport.documentSeries,
                    dto.passport.documentNumber,
                    dto.passport.placeOfIssue,
                    SimpleDateFormat(
                        "yyyy-MM-dd", Locale.US
                    ).parse(dto.passport.dateOfIssue),
                    null,
                    null,
                    null,
                    null
                )
            }

            @JvmStatic
            fun fromPatientDto(dto: PatientProfileDto): Profile {
                return Profile(
                    dto.lastName,
                    dto.firstName,
                    dto.middleName,
                    dto.gender,
                    dto.email,
                    dto.birthday,
                    PhoneNumber(dto.phoneNumber.fullNumber),
                    dto.passport.documentSeries,
                    dto.passport.documentNumber,
                    dto.passport.placeOfIssue,
                    SimpleDateFormat(
                        "yyyy-MM-dd", Locale.US
                    ).parse(dto.passport.dateOfIssue),
                    dto.snils,
                    dto.address.region,
                    dto.address.city,
                    dto.address.street
                )
            }
        }


        private fun checkIfDateError(dateString: String): Date? {
            try {
                val date = SimpleDateFormat(
                    "yyyy-MM-dd", Locale.US
                ).parse(dateString)

                if (date != null) {
                    return date
                }
            } catch (ex: Exception) {
                println("Date parse exception: $ex")
            }
            return null
        }
    }
}