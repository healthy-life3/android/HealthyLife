package ru.nsu.healthylife.models

import android.telephony.PhoneNumberUtils

data class PhoneNumber(
    var fullNumber: String
) {
    lateinit var countryCode: String
    lateinit var subscriberNumber: String

    fun tryInit(): Boolean {
        val formatted = PhoneNumberUtils.formatNumberToE164(fullNumber, "RU")
        if (formatted != null) {
            fullNumber = formatted
            countryCode =
                fullNumber.substring(fullNumber.indices.first..fullNumber.indices.first /*+ 1*/)
            subscriberNumber =
                fullNumber.substring(fullNumber.indices.first + 1 /*2*/..fullNumber.indices.last)
            return true
        }
        return false
    }
}