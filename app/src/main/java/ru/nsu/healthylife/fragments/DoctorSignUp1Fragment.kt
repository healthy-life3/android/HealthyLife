package ru.nsu.healthylife.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import ru.nsu.healthylife.R
import ru.nsu.healthylife.models.RegistrationModel

class DoctorSignUp1Fragment : Fragment() {
    private lateinit var viewModel: RegistrationModel
    private lateinit var container: ViewGroup

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProvider(requireActivity()).get(
            RegistrationModel::class.java
        )
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        if (container != null) {
            this.container = container
        }
        // Inflate the layout for this fragment
        return inflater.inflate(
            R.layout.doctor_sign_up_page_1_layout,
            container,
            false
        )
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val firstNameEditText =
            view.findViewById<EditText>(R.id.firstNameEditText)
        val middleNameEditText =
            view.findViewById<EditText>(R.id.middleNameEditText)
        val lastNameEditText =
            view.findViewById<EditText>(R.id.lastNameEditText)
        //TODO gender
        val genderSpinner =
            view.findViewById<Spinner>(R.id.genderSpinner)
        ArrayAdapter.createFromResource(
            requireContext(),
            R.array.array_gender,
            android.R.layout.simple_spinner_item
        ).also { adapter ->
            // Specify the layout to use when the list of choices appears
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            // Apply the adapter to the spinner
            genderSpinner.adapter = adapter
        }
        viewModel.setGenderSpinnerListener(genderSpinner)
        val birthdayEditText =
            view.findViewById<EditText>(R.id.birthdayEditText)
        val phoneNumberEditText =
            view.findViewById<EditText>(R.id.phoneEditText)
        val emailEditText =
            view.findViewById<EditText>(R.id.emailEditText)
        view.findViewById<Button>(R.id.nextButton)
            .setOnClickListener {
                var hasError = checkIfBlankAny(
                    firstNameEditText to "First name",
//                    middleNameEditText to "Middle name",
                    lastNameEditText to "Last name",
                    phoneNumberEditText to "Phone number",
                    emailEditText to "Email"
                )
                hasError = viewModel.setIfCorrectBirthday(
                    birthdayEditText,
                    hasError
                )
                hasError = viewModel.setIfCorrectPhoneNumber(
                    phoneNumberEditText,
                    hasError
                )
                if (!hasError) {
                    viewModel.setContactInfo(
                        firstNameEditText.text.toString().trim(),
                        middleNameEditText.text.toString().trim(),
                        lastNameEditText.text.toString().trim(),
                        emailEditText.text.toString().trim()
                    )
                    replaceFragment(
                        container.id,
                        DoctorSignUp2Fragment.newInstance()
                    )
                }
            }
    }

    private fun replaceFragment(containerViewId: Int, fragment: Fragment) {
        parentFragmentManager
            .beginTransaction()
            .replace(containerViewId, fragment)
            .setReorderingAllowed(true)
            .addToBackStack(null)
            .commit()
    }

    private fun checkIfBlank(
        name: EditText,
        title: String,
        hadError: Boolean
    ): Boolean {
        if (name.text.isBlank()) {
            name.error = "$title can't be empty"
            return true
        }
        return hadError
    }

    private fun checkIfBlankAny(vararg fields: Pair<EditText, String>): Boolean {
        var hasError = false
        fields.forEach {
            hasError = checkIfBlank(it.first, it.second, hasError)
        }
        return hasError
    }

    companion object {
        const val FIRST_NAME_KEY = "first_name"
        const val MIDDLE_NAME_KEY = "middle_name"
        const val LAST_NAME_KEY = "last_name"

        @JvmStatic
        fun newInstance() = DoctorSignUp1Fragment()
    }
}