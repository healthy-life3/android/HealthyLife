package ru.nsu.healthylife.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import ru.nsu.healthylife.R
import ru.nsu.healthylife.db.JwtTokenEntity
import ru.nsu.healthylife.models.Session
import kotlin.concurrent.thread

@AndroidEntryPoint
class WelcomeFragment : Fragment() {
    private lateinit var session: Session
    private lateinit var container: ViewGroup

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        session = ViewModelProvider(requireActivity()).get(
            Session::class.java
        )
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        if (container != null) {
            this.container = container
        }
        // Inflate the layout for this fragment
        return inflater.inflate(
            R.layout.welcome_layout,
            container,
            false
        )
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        view.findViewById<Button>(R.id.welcomeDoctorButton)
            .setOnClickListener {
                replaceFragment(
                    container.id,
                    DoctorSignInFragment.newInstance()
                )
            }
        view.findViewById<Button>(R.id.welcomePatientButton)
            .setOnClickListener {
                replaceFragment(
                    container.id,
                    PatientSignInFragment.newInstance()
                )
            }
        lifecycleScope.launch {
            println("Trying to get Access")
            val thread = thread {
                if (session.tryGetAccess()) {
                    println("Success")
                    replaceFragment(
                        container.id,
                        when (session.type) {
                            JwtTokenEntity.DOCTOR_TYPE -> DoctorInfoFragment.newInstance()
                            JwtTokenEntity.PATIENT_TYPE -> PatientInfoFragment.newInstance()
                            else -> newInstance()
                        }
                    )
                }
            }
            withContext(Dispatchers.IO) {
                thread.join()
            }
        }
    }

    private fun replaceFragment(containerViewId: Int, fragment: Fragment) {
        parentFragmentManager
            .beginTransaction()
            .replace(containerViewId, fragment)
            .setReorderingAllowed(true)
            .addToBackStack(null)
            .commit()
    }

    companion object {
        @JvmStatic
        fun newInstance() = WelcomeFragment()
    }
}