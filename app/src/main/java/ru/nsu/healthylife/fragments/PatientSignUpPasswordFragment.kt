package ru.nsu.healthylife.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch
import ru.nsu.healthylife.Constants
import ru.nsu.healthylife.R
import ru.nsu.healthylife.models.RegistrationModel
import ru.nsu.healthylife.services.AuthenticationService
import javax.inject.Inject

@AndroidEntryPoint
class PatientSignUpPasswordFragment : Fragment() {
    private lateinit var viewModel: RegistrationModel
    private lateinit var container: ViewGroup

    @Inject lateinit var authenticationService: AuthenticationService

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProvider(requireActivity()).get(
            RegistrationModel::class.java
        )
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        if (container != null) {
            this.container = container
        }
        // Inflate the layout for this fragment
        return inflater.inflate(
            R.layout.patient_sign_up_password_layout,
            container,
            false
        )
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val newPasswordEditText =
            view.findViewById<EditText>(R.id.newPasswordEditText)
        val repeatPasswordEditText =
            view.findViewById<EditText>(R.id.repeatPasswordEditText)
        view.findViewById<Button>(R.id.signUpButton)
            .setOnClickListener {
                if (checkPasswords(
                        newPasswordEditText,
                        repeatPasswordEditText
                    )
                ) {
                    //TODO: sign up
                    viewModel.password = newPasswordEditText.text.toString()
                    viewLifecycleOwner.lifecycleScope.launch {
                        viewModel.tryPatientRegistration(
                            authenticationService,
                            container.id,
                            WelcomeFragment.newInstance(),
                            repeatPasswordEditText
                        )
                    }
                }
            }
    }

    private fun replaceFragment(containerViewId: Int, fragment: Fragment) {
        parentFragmentManager
            .beginTransaction()
            .replace(containerViewId, fragment)
            .setReorderingAllowed(true)
            .addToBackStack(null)
            .commit()
    }

    private fun checkPasswords(
        passwordEditText: EditText,
        repeatPasswordEditText: EditText
    ): Boolean {
        return passwordEditText.text.isNotBlank() &&
                repeatPasswordEditText.text.isNotBlank() &&
                passwordEditText.text.toString() == repeatPasswordEditText.text.toString()
    }

    private fun reactOnStatusCode(statusCode: Int, field: EditText) {
        field.error = when (statusCode) {
            Constants.BAD_REQUEST_CODE -> Constants.BAD_REQUEST
            Constants.INVALID_CREDENTIALS_CODE -> Constants.INVALID_CREDENTIALS
            Constants.FORBIDDEN_CODE -> Constants.FORBIDDEN
            Constants.NOT_FOUND_CODE -> Constants.NOT_FOUND
            Constants.SERVER_ERROR_CODE -> Constants.SERVER_ERROR
            else -> Constants.SERVER_ERROR
        }
    }

    companion object {
        @JvmStatic
        fun newInstance() = PatientSignUpPasswordFragment()
    }
}