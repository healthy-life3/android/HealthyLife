package ru.nsu.healthylife.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch
import ru.nsu.healthylife.R
import ru.nsu.healthylife.models.RegistrationModel
import ru.nsu.healthylife.models.Session
import ru.nsu.healthylife.services.AuthenticationService
import javax.inject.Inject

@AndroidEntryPoint
class DoctorSignInFragment : Fragment() {
    private lateinit var viewModel: RegistrationModel
    private lateinit var session: Session
    private lateinit var container: ViewGroup

    @Inject lateinit var authenticationService: AuthenticationService

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProvider(requireActivity()).get(
            RegistrationModel::class.java
        )
        session = ViewModelProvider(requireActivity()).get(
            Session::class.java
        )
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        if (container != null) {
            this.container = container
        }
        // Inflate the layout for this fragment
        return inflater.inflate(
            R.layout.doctor_sign_in_layout,
            container,
            false
        )
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.type = RegistrationModel.DOCTOR_TYPE
        val loginEditText =
            view.findViewById<EditText>(R.id.emailEditText)
        val passwordEditText =
            view.findViewById<EditText>(R.id.passwordEditText)
        view.findViewById<Button>(R.id.doctorSignInButton)
            .setOnClickListener {
                if (loginEditText.text.isNotBlank() &&
                    passwordEditText.text.isNotEmpty()
                ) {
                    viewModel.login = loginEditText.text.toString()
                    viewModel.password = passwordEditText.text.toString()
                    //TODO: authentication
                    viewLifecycleOwner.lifecycleScope.launch {
                        viewModel.tryDoctorAuth(
                            session,
                            container.id,
                            DoctorInfoFragment.newInstance(),
                            passwordEditText
                        )
                    }
                }
            }
        view.findViewById<TextView>(R.id.toRegisterTextView)
            .setOnClickListener {
                viewModel.type = RegistrationModel.DOCTOR_TYPE
                replaceFragment(
                    container.id,
                    DoctorSignUp1Fragment.newInstance()
                )
            }

    }

    private fun replaceFragment(containerViewId: Int, fragment: Fragment) {
        parentFragmentManager
            .beginTransaction()
            .replace(containerViewId, fragment)
            .setReorderingAllowed(true)
            .addToBackStack(null)
            .commit()
    }


    companion object {
        @JvmStatic
        fun newInstance() = DoctorSignInFragment()
    }
}