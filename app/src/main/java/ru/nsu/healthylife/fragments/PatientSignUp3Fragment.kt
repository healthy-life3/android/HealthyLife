package ru.nsu.healthylife.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import ru.nsu.healthylife.R
import ru.nsu.healthylife.models.RegistrationModel

class PatientSignUp3Fragment : Fragment() {
    private lateinit var viewModel: RegistrationModel
    private lateinit var container: ViewGroup

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProvider(requireActivity()).get(
            RegistrationModel::class.java
        )
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        if (container != null) {
            this.container = container
        }
        // Inflate the layout for this fragment
        return inflater.inflate(
            R.layout.patient_sign_up_page_3_layout,
            container,
            false
        )
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val regionEditText =
            view.findViewById<EditText>(R.id.areaEditText)
        val cityEditText =
            view.findViewById<EditText>(R.id.cityEditText)
        val streetEditText =
            view.findViewById<EditText>(R.id.streetEditText)
        val houseNumberEditText =
            view.findViewById<EditText>(R.id.buildingEditText)
        val apartmentNumberEditText =
            view.findViewById<EditText>(R.id.apartmentEditText)
        view.findViewById<Button>(R.id.nextButton)
            .setOnClickListener {
                val hasError = checkForError(regionEditText to "Region,",
                cityEditText to "City",
                streetEditText to "Street",
                houseNumberEditText to "House number",
                apartmentNumberEditText to "Apartment number")
                if (!hasError) {
                    viewModel.region = regionEditText.text.toString()
                    viewModel.city = cityEditText.text.toString()
                    viewModel.street = streetEditText.text.toString()
                    viewModel.houseNumber = houseNumberEditText.text.toString()
                    viewModel.apartmentNumber =
                        houseNumberEditText.text.toString().toInt()
                    replaceFragment(
                        container.id,
                        PatientSignUpPasswordFragment.newInstance()
                    )
                }
            }
    }

    private fun replaceFragment(containerViewId: Int, fragment: Fragment) {
        parentFragmentManager
            .beginTransaction()
            .replace(containerViewId, fragment)
            .setReorderingAllowed(true)
            .addToBackStack(null)
            .commit()
    }

    private fun checkIfBlank(
        name: EditText,
        title: String,
        hadError: Boolean
    ): Boolean {
        if (name.text.isBlank()) {
            name.error = "$title can't be empty"
            return true
        }
        return hadError
    }

    private fun checkForError(vararg fields: Pair<EditText, String>): Boolean {
        var hasError = false
        fields.forEach {
            hasError = checkIfBlank(it.first, it.second, hasError)
        }
        return hasError
    }

    companion object {
        @JvmStatic
        fun newInstance() = PatientSignUp3Fragment()
    }
}