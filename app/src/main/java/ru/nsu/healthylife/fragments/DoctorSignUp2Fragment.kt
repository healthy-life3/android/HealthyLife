package ru.nsu.healthylife.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import ru.nsu.healthylife.R
import ru.nsu.healthylife.models.RegistrationModel

class DoctorSignUp2Fragment : Fragment() {
    private lateinit var viewModel: RegistrationModel
    private lateinit var container: ViewGroup

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProvider(requireActivity()).get(
            RegistrationModel::class.java
        )
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        if (container != null) {
            this.container = container
        }
        // Inflate the layout for this fragment
        return inflater.inflate(
            R.layout.doctor_sign_up_page_2_layout,
            container,
            false
        )
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val documentSeriesEditText =
            view.findViewById<EditText>(R.id.passportSeriesEditText)
        val documentNumberEditText =
            view.findViewById<EditText>(R.id.passportNumberEditText)
        val placeOfIssueEditText =
            view.findViewById<EditText>(R.id.passportIssueByEditText)
        val dateOfIssueEditText =
            view.findViewById<EditText>(R.id.dateOfIssueEditText)
        view.findViewById<Button>(R.id.nextButton)
            .setOnClickListener {
                var hasError = checkForError(
                    documentSeriesEditText to "Document series",
                    documentNumberEditText to "Document number",
                    placeOfIssueEditText to "Place of issue"
                )
                hasError = viewModel.setIfCorrectDateOfIssue(
                    dateOfIssueEditText, hasError
                )
                if (!hasError) {
                    viewModel.documentSeries =
                        documentSeriesEditText.text.toString()
                    viewModel.documentNumber =
                        documentNumberEditText.text.toString()
                    viewModel.placeOfIssue =
                        placeOfIssueEditText.text.toString()
                    replaceFragment(
                        container.id,
                        DoctorSignUpPasswordFragment.newInstance()
                    )
                }
            }
    }

    private fun replaceFragment(containerViewId: Int, fragment: Fragment) {
        parentFragmentManager
            .beginTransaction()
            .replace(containerViewId, fragment)
            .setReorderingAllowed(true)
            .addToBackStack(null)
            .commit()
    }

    private fun checkIfBlank(
        name: EditText,
        title: String,
        hadError: Boolean
    ): Boolean {
        if (name.text.isBlank()) {
            name.error = "$title can't be empty"
            return true
        }
        return hadError
    }

    private fun checkForError(vararg fields: Pair<EditText, String>): Boolean {
        var hasError = false
        fields.forEach {
            hasError = checkIfBlank(it.first, it.second, hasError)
        }
        return hasError
    }

    companion object {
        @JvmStatic
        fun newInstance() = DoctorSignUp2Fragment()
    }
}