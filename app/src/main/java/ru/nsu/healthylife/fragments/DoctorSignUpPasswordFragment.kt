package ru.nsu.healthylife.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch
import ru.nsu.healthylife.Constants
import ru.nsu.healthylife.R
import ru.nsu.healthylife.models.RegistrationModel
import ru.nsu.healthylife.services.AuthenticationService
import javax.inject.Inject

@AndroidEntryPoint
class DoctorSignUpPasswordFragment : Fragment() {
    private lateinit var viewModel: RegistrationModel
    private lateinit var container: ViewGroup

    @Inject lateinit var authenticationService: AuthenticationService

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProvider(requireActivity()).get(
            RegistrationModel::class.java
        )
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        if (container != null) {
            this.container = container
        }
        // Inflate the layout for this fragment
        return inflater.inflate(
            R.layout.doctor_sign_up_password_layout,
            container,
            false
        )
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val newPasswordEditText =
            view.findViewById<EditText>(R.id.newPasswordEditText)
        val repeatPasswordEditText =
            view.findViewById<EditText>(R.id.repeatPasswordEditText)
        view.findViewById<Button>(R.id.signUpButton)
            .setOnClickListener {
                if (newPasswordEditText.text.isNotEmpty() &&
                    repeatPasswordEditText.text.isNotEmpty() &&
                    newPasswordEditText.text.toString() == repeatPasswordEditText.text.toString()
                ) {
                    //TODO: sign up
                    viewModel.password = newPasswordEditText.text.toString()
                    viewLifecycleOwner.lifecycleScope.launch {
                        viewModel.tryDoctorRegistration(
                            authenticationService,
                            container.id,
                            WelcomeFragment.newInstance(),
                            repeatPasswordEditText
                        )
                    }
                } else {
                    repeatPasswordEditText.error = Constants.NOT_EQUAL_PASSWORDS
                }
            }
    }

    private fun replaceFragment(containerViewId: Int, fragment: Fragment) {
        parentFragmentManager
            .beginTransaction()
            .replace(containerViewId, fragment)
            .setReorderingAllowed(true)
            .addToBackStack(null)
            .commit()
    }

    companion object {
        @JvmStatic
        fun newInstance() = DoctorSignUpPasswordFragment()
    }
}