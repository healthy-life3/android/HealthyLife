package ru.nsu.healthylife.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import ru.nsu.healthylife.R
import ru.nsu.healthylife.models.RegistrationModel
import ru.nsu.healthylife.models.Session
import kotlin.concurrent.thread

class PatientInfoFragment : Fragment() {
    private lateinit var viewModel: RegistrationModel
    private lateinit var container: ViewGroup
    private lateinit var session: Session

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProvider(requireActivity()).get(
            RegistrationModel::class.java
        )
        session = ViewModelProvider(requireActivity()).get(
            Session::class.java
        )
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        if (container != null) {
            this.container = container
        }
        // Inflate the layout for this fragment
        return inflater.inflate(
            R.layout.patient_info_layout,
            container,
            false
        )
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        view.findViewById<Button>(R.id.backButton)
            .setOnClickListener {
                lifecycleScope.launch {
                    val thread = thread {
                        session.dropAccess()
                    }
                    withContext(Dispatchers.IO) {
                        thread.join()
                    }
                }
                parentFragmentManager.let { for (i in 1..it.backStackEntryCount) it.popBackStack() }
                replaceFragment(container.id, WelcomeFragment.newInstance())
            }
        view.findViewById<Button>(R.id.profileButton)
            .setOnClickListener {
                replaceFragment(
                    container.id,
                    PatientProfileFragment.newInstance()
                )
            }
        view.findViewById<Button>(R.id.docsButton)
            .setOnClickListener {
                replaceFragment(
                    container.id,
                    PatientDocsFragment.newInstance()
                )
            }
        view.findViewById<Button>(R.id.addressButton)
            .setOnClickListener {
                replaceFragment(
                    container.id,
                    PatientAddressFragment.newInstance()
                )
            }
        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            val nameEditText = view.findViewById<EditText>(R.id.fullNameEditText)
            nameEditText.post {
                session.setName(nameEditText)
            }
        }
    }

    private fun replaceFragment(containerViewId: Int, fragment: Fragment) {
        parentFragmentManager
            .beginTransaction()
            .replace(containerViewId, fragment)
            .setReorderingAllowed(true)
            .addToBackStack(null)
            .commit()
    }

    companion object {
        @JvmStatic
        fun newInstance() = PatientInfoFragment()
    }
}