package ru.nsu.healthylife.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query

@Dao
interface JwtTokenDao {
    @Insert
    fun storeToken(token: JwtTokenEntity)

    @Query("SELECT * FROM tokens")
    fun loadTokens() : List<JwtTokenEntity>

    @Query("DELETE FROM tokens")
    fun deleteTokens()
}