package ru.nsu.healthylife.db

import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverter
import androidx.room.TypeConverters
import kotlinx.serialization.Serializable
import ru.nsu.healthylife.dto.JwtToken

@Serializable
@Entity(tableName = "tokens")
@TypeConverters(JwtTokenEntity.JwtTokenEntityConverter::class)
data class JwtTokenEntity(@PrimaryKey var token: JwtToken, var type: Int) {
    companion object {
        const val DOCTOR_TYPE = 0
        const val PATIENT_TYPE = 1
    }

    class JwtTokenEntityConverter {
        @TypeConverter
        fun toEntity(tokenString: String) : JwtToken {
            return JwtToken(tokenString)
        }
        @TypeConverter
        fun fromEntity(token: JwtToken) : String {
            return token.token
        }
    }
}