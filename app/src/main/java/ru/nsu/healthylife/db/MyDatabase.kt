package ru.nsu.healthylife.db

import androidx.room.Database
import androidx.room.RoomDatabase

@Database(entities = [JwtTokenEntity::class], version = 1)
abstract class MyDatabase : RoomDatabase() {
    abstract fun jwtTokenDao() : JwtTokenDao
}