package ru.nsu.healthylife.db

import android.content.Context
import androidx.room.Room
import dagger.hilt.android.qualifiers.ApplicationContext
import ru.nsu.healthylife.dto.JwtToken
import javax.inject.Inject

class DBRepository @Inject constructor(@ApplicationContext context: Context) {
    private var db : MyDatabase

    init {
        db = Room.databaseBuilder(
            context,
            MyDatabase::class.java, "tokens"
        ).build()
    }


    fun getToken() : JwtTokenEntity? {
        val tokens = db.jwtTokenDao().loadTokens()
        return tokens.firstOrNull()
    }

    fun storeDoctorToken(token: JwtToken) {
        val t = JwtTokenEntity(token, JwtTokenEntity.DOCTOR_TYPE)
        storeToken(t)
    }

    fun storePatientToken(token: JwtToken) {
        val t = JwtTokenEntity(token, JwtTokenEntity.PATIENT_TYPE)
        storeToken(t)
    }

    private fun storeToken(token: JwtTokenEntity) {
        db.jwtTokenDao().storeToken(token)
    }

    fun deleteTokens() {
        db.jwtTokenDao().deleteTokens()
    }
}