package ru.nsu.healthylife.dto

import kotlinx.serialization.Serializable

@Serializable
data class AddressDto(
    var region: String,
    var city: String,
    var street: String,
    var zipcode: String,
    var houseNumber: String,
    var apartmentNumber: Int
)
