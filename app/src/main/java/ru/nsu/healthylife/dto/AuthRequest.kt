package ru.nsu.healthylife.dto

import android.util.Base64

data class AuthRequest(
    var login: String,
    var password: String
) {
    fun serialize(): String {
        return Base64.encodeToString(
            "${login}:${password}".encodeToByteArray(),
            Base64.NO_WRAP
        )
    }
}