package ru.nsu.healthylife.dto

import kotlinx.serialization.Serializable

@Serializable
data class DoctorRegistrationRequest(
    var lastName : String,
    var firstName: String,
    var middleName : String,

    var phoneNumber: PhoneNumberDto,

    var birthday : String,
    var password : String,
    var email : String,
    var gender : String,

    var passport: PassportDto,
    var profession: DoctorProfessionDto?
)