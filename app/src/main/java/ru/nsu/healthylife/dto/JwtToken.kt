package ru.nsu.healthylife.dto

import androidx.room.PrimaryKey
import kotlinx.serialization.Serializable

@Serializable
data class JwtToken(@PrimaryKey var token: String)
