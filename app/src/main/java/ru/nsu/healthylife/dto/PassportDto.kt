package ru.nsu.healthylife.dto

import kotlinx.serialization.Serializable

@Serializable
data class PassportDto(
    var documentSeries: String,
    var documentNumber: String,
    var placeOfIssue: String,
    /**
     * This is date-format YYYY-MM-DDThh:mm:ss
     * */
    var dateOfIssue: String
)
