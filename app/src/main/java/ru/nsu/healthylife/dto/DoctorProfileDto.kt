package ru.nsu.healthylife.dto

import kotlinx.serialization.Serializable

@Serializable
data class DoctorProfileDto(
    var lastName: String,
    var firstName: String,
    var middleName: String,

    var gender: String,
    var email: String,
    var birthday: String,

    var phoneNumber: PhoneNumberDto,
    var passport: PassportDto,
    var profession: DoctorProfessionDto?
)
