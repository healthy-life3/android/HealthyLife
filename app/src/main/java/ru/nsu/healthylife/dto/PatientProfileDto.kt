package ru.nsu.healthylife.dto

import kotlinx.serialization.Serializable

@Serializable
data class PatientProfileDto(
    var lastName: String,
    var firstName: String,
    var middleName: String,

    var gender: String,
    var email: String,
    var birthday: String,

    var phoneNumber: PhoneNumberDto,
    var passport: PassportDto,
    var snils: String,
    var address: AddressDto
)
