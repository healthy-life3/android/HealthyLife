package ru.nsu.healthylife.dto

import kotlinx.serialization.Serializable

@Serializable
data class DoctorProfessionDto(var  id: Int, var name : String)
