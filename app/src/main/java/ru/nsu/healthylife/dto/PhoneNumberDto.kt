package ru.nsu.healthylife.dto

import kotlinx.serialization.Serializable

@Serializable
data class PhoneNumberDto(
    var fullNumber: String,
    var countryCode: String,
    var subscriberNumber: String
)