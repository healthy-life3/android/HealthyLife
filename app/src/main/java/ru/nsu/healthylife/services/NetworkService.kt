package ru.nsu.healthylife.services

import com.android.volley.Response
import org.json.JSONObject
import ru.nsu.healthylife.dto.JwtToken
import javax.inject.Inject

class NetworkService @Inject constructor(
    private var networkRepository : NetworkRepository
) {
    fun loadDoctorProfile(
        token: JwtToken,
        responseListener: Response.Listener<JSONObject>,
        errorListener: Response.ErrorListener?
    ) {
        networkRepository.loadDoctorProfile(
            token, responseListener, errorListener
        )
    }

    fun loadPatientProfile(
        token: JwtToken,
        responseListener: Response.Listener<JSONObject>,
        errorListener: Response.ErrorListener?
    ) {
        networkRepository.loadPatientProfile(
            token, responseListener, errorListener
        )
    }
}