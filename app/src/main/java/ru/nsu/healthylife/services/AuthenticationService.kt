package ru.nsu.healthylife.services

import com.android.volley.Response
import org.json.JSONObject
import ru.nsu.healthylife.dto.AuthRequest
import ru.nsu.healthylife.dto.DoctorRegistrationRequest
import ru.nsu.healthylife.dto.PatientRegistrationRequest
import javax.inject.Inject

class AuthenticationService @Inject constructor(
    private val  networkRepository : NetworkRepository
) {
//    @Inject lateinit var  networkRepository : NetworkRepository

    fun sendPatientAuthRequest(
        request: AuthRequest,
        responseListener: Response.Listener<String>,
        errorListener: Response.ErrorListener?
    ) {
        networkRepository.sendPatientAuthRequest(
            request, responseListener, errorListener
        )
    }

    fun sendDoctorAuthRequest(
        request: AuthRequest,
        responseListener: Response.Listener<String>,
        errorListener: Response.ErrorListener?
    ) {
        networkRepository.sendDoctorAuthRequest(
            request, responseListener, errorListener
        )
    }

    fun sendPatientRegistrationRequest(
        request: PatientRegistrationRequest,
        responseListener: Response.Listener<JSONObject>,
        errorListener: Response.ErrorListener?
    ) {
        networkRepository.sendPatientRegistrationRequest(
            request, responseListener, errorListener
        )
    }

    fun sendDoctorRegistrationRequest(
        request: DoctorRegistrationRequest,
        responseListener: Response.Listener<JSONObject>,
        errorListener: Response.ErrorListener?
    ) {
        networkRepository.sendDoctorRegistrationRequest(
            request, responseListener, errorListener
        )
    }

}