package ru.nsu.healthylife.services

import android.content.Context
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import org.json.JSONObject
import ru.nsu.healthylife.dto.AuthRequest
import ru.nsu.healthylife.dto.DoctorRegistrationRequest
import ru.nsu.healthylife.dto.JwtToken
import ru.nsu.healthylife.dto.PatientRegistrationRequest
import javax.inject.Inject

class NetworkRepository @Inject constructor(
    @ApplicationContext
    context: Context
) {
    companion object {
//        const val SERVER_URL = "http://192.168.0.26:8080"
        const val SERVER_URL = "http://localhost:8080"
    }


    private val patientUrl = "$SERVER_URL/patients"
    private val patientAuthUrl = "$patientUrl/auth"
    private val patientRegistrationUrl = "$patientUrl/register"
    private val patientProfileUrl = "$patientUrl/profile"

    private val doctorUrl = "$SERVER_URL/doctors"
    private val doctorAuthUrl = "$doctorUrl/auth"
    private val doctorRegistrationUrl = "$doctorUrl/register"
    private val doctorProfileUrl = "$doctorUrl/profile"

    private val requestQueue: RequestQueue by lazy {
        Volley.newRequestQueue(context.applicationContext)
    }

    fun sendPatientAuthRequest(
        request: AuthRequest,
        responseListener: Response.Listener<String>,
        errorListener: Response.ErrorListener?
    ) {
        val authStringRequest = AuthStringRequest(
            Request.Method.POST, patientAuthUrl,
            responseListener,
            errorListener,
            request
        )
        //TODO remove logging
        println("""Sent patient auth request: "$authStringRequest."""")
        //TODO patient auth
        requestQueue.add(authStringRequest)
    }

    fun sendDoctorAuthRequest(
        request: AuthRequest,
        responseListener: Response.Listener<String>,
        errorListener: Response.ErrorListener?
    ) {
        val authStringRequest = AuthStringRequest(
            Request.Method.POST, doctorAuthUrl,
            responseListener,
            errorListener,
            request
        )
        //TODO remove logging
        println("""Sent doctor auth request: "$authStringRequest"""")
        println("""Headers: ${authStringRequest.headers}""")
        //TODO doctor auth
        requestQueue.add(authStringRequest)
    }

    fun sendPatientRegistrationRequest(
        request: PatientRegistrationRequest,
        responseListener: Response.Listener<JSONObject>,
        errorListener: Response.ErrorListener?
    ) {
        val jsonObject = JSONObject(Json.encodeToString(request))
        val jsonRequest = JsonObjectRequest(
            Request.Method.POST, patientRegistrationUrl,
            jsonObject, responseListener, errorListener
        )
        //TODO remove logging
        println("""Sent patient registration request: "$jsonRequest"""")
        requestQueue.add(jsonRequest)
    }

    fun sendDoctorRegistrationRequest(
        request: DoctorRegistrationRequest,
        responseListener: Response.Listener<JSONObject>,
        errorListener: Response.ErrorListener?
    ) {
        val jsonObject = JSONObject(Json.encodeToString(request))
        val jsonRequest = JsonObjectRequest(
            Request.Method.POST, doctorRegistrationUrl,
            jsonObject, responseListener, errorListener
        )
        //TODO remove logging
        println("""Sent doctor registration request: "$jsonRequest"""")
        requestQueue.add(jsonRequest)
    }

    fun loadDoctorProfile(
        token: JwtToken,
        responseListener: Response.Listener<JSONObject>,
        errorListener: Response.ErrorListener?
    ) {
        loadProfile(token, doctorProfileUrl, responseListener, errorListener)
    }

    fun loadPatientProfile(
        token: JwtToken,
        responseListener: Response.Listener<JSONObject>,
        errorListener: Response.ErrorListener?
    ) {
        loadProfile(token, patientProfileUrl, responseListener, errorListener)
    }

    private fun loadProfile(
        token: JwtToken,
        url: String,
        responseListener: Response.Listener<JSONObject>,
        errorListener: Response.ErrorListener?
    ) {
        val request = AuthenticatedJSONObjectRequest(
            Request.Method.GET, url,
            responseListener, errorListener, token
        )
        //TODO remove logging
        println("""Sent profile loading request: "$request"""")
        println("""JwtToken: "$token"""")
        requestQueue.add(request)
    }

    class AuthStringRequest(
        method: Int, url: String,
        listener: Response.Listener<String>,
        errorListener: Response.ErrorListener?,
        private val credentials : AuthRequest
    ) : StringRequest(method, url, listener, errorListener) {
        override fun getHeaders(): MutableMap<String, String> {
            val headers = HashMap<String, String>()
            headers["Content-Type"] = "application/json"
            headers["Authorization"] = "Basic ${credentials.serialize()}"
            return headers
        }
    }
    class AuthenticatedJSONObjectRequest(
        method: Int, url: String,
        listener: Response.Listener<JSONObject>,
        errorListener: Response.ErrorListener?,
        private val credentials : JwtToken
    ) : JsonObjectRequest(method, url, null,  listener, errorListener) {
        override fun getHeaders(): MutableMap<String, String> {
            val headers = HashMap<String, String>()
            headers["Content-Type"] = "application/json"
            headers["Authorization"] = "Bearer ${credentials.token}"
            return headers
        }
    }
}